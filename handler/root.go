package handler

import (
	"net/http"

	"gitlab.com/inquizarus/rest"
)

// MakeRootHandlers all root handlers
func MakeRootHandlers() []*rest.BaseHandler {
	return []*rest.BaseHandler{
		{
			Path: "/",
			Name: "root-route",
			Get: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
				w.Write([]byte("{\"status\":\"ok\"}"))
			},
		},
	}
}
