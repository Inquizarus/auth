package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/inquizarus/rest"
)

func authenticateApp(w http.ResponseWriter, r *http.Request, vars map[string]string) {
	id := vars["id"]
	w.Write([]byte(fmt.Sprintf("{\"status\":\"app with id %s is logged in\"}", id)))
}

// MakeAppHandlers returns all handlers related to app things
func MakeAppHandlers() []*rest.BaseHandler {
	return []*rest.BaseHandler{
		{
			Path: "/auth/app/{id}",
			Name: "authenticate-app-route",
			Post: authenticateApp,
		},
	}
}
