package handler_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/inquizarus/auth/datasource"

	"gitlab.com/inquizarus/auth/handler"
)

func TestUserLoginPostWithLackingCredentials(t *testing.T) {
	ur := datasource.MakeUserJSONReader("./test_data/users.json")
	h := handler.MakeUserLoginHandler(ur)
	r, _ := http.NewRequest("POST", "/", strings.NewReader(""))
	w := httptest.NewRecorder()
	h.Handle(w, r)

	if w.Code != http.StatusUnauthorized {
		t.Error("lacking credentials did not result in unauthorized result")
	}

	bodyBytes, _ := ioutil.ReadAll(w.Body)
	response, err := handler.MakeUserLoginResponse(bodyBytes)

	if nil != err {
		t.Errorf("error when parsing user login response \"%s\"", err)
	}

	if response.Token != "" {
		t.Errorf("response token where not empty after unsuccessful login")
	}
}

func TestUserLoginPostWithInvalidCredentials(t *testing.T) {
	ur := datasource.MakeUserJSONReader("./test_data/users.json")
	b, _ := json.Marshal(handler.MakeUserLoginBody("invalid", "abc123"))
	h := handler.MakeUserLoginHandler(ur)
	r, _ := http.NewRequest("POST", "/", bytes.NewReader(b))
	w := httptest.NewRecorder()
	h.Handle(w, r)

	if w.Code != http.StatusUnauthorized {
		t.Error("invalid credentials did not result in unauthorized result")
	}

	bodyBytes, _ := ioutil.ReadAll(w.Body)
	response, err := handler.MakeUserLoginResponse(bodyBytes)

	if nil != err {
		t.Errorf("error when parsing user login response \"%s\"", err)
	}

	if response.Token != "" {
		t.Errorf("response token where not empty after unsuccessful login")
	}
}

func TestUserLoginPostWithValidCredentials(t *testing.T) {
	ur := datasource.MakeUserJSONReader("./test_data/users.json")
	b, _ := json.Marshal(handler.MakeUserLoginBody("test", "password"))
	h := handler.MakeUserLoginHandler(ur)
	r, _ := http.NewRequest("POST", "/", bytes.NewReader(b))
	w := httptest.NewRecorder()
	h.Handle(w, r)

	if w.Code != http.StatusOK {
		t.Error("valid credentials did not result in ok result")
	}

	bodyBytes, _ := ioutil.ReadAll(w.Body)
	response, err := handler.MakeUserLoginResponse(bodyBytes)

	if nil != err {
		t.Errorf("error when parsing user login response \"%s\"", err)
	}

	if response.Token == "" {
		t.Errorf("response token where empty after successful login")
	}

}
