package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/inquizarus/auth/model"
	"gitlab.com/inquizarus/auth/security"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/inquizarus/auth/datasource"
	"gitlab.com/inquizarus/rest"
)

// LoginBody for user validation
type LoginBody struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// LoginResponse for when doing user login
type LoginResponse struct {
	Token string `json:"token"`
}

func makeLoginUserAction(ur datasource.UserReader) func(http.ResponseWriter, *http.Request, map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
		response := LoginResponse{}
		bodyBytes, _ := ioutil.ReadAll(r.Body)
		body := LoginBody{}
		json.Unmarshal(bodyBytes, &body)

		if body.Password == "" || body.Username == "" {
			setUnauthorized(w)
		}

		user, err := ur.GetByUsername(body.Username)

		if nil != err {
			setUnauthorized(w)
		}
		isSamePassword := security.VerifyPassword([]byte(user.Password), []byte(body.Password))
		if nil == err && true == isSamePassword {
			now := time.Now()
			exp := now.Add(time.Minute * 60)
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"iss": "auth.service",
				"iat": now.Unix(),
				"exp": exp.Unix(),
				"usr": map[string]string{
					"site":     r.Host,
					"username": user.Username,
				},
			})
			tokenString, err := token.SignedString([]byte(getJwtTokenKey())) // make sign key non static
			if nil != err {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				response.Token = tokenString
			}
		}

		if true != isSamePassword {
			setUnauthorized(w)
		}

		responseBytes, _ := json.Marshal(response)
		w.Write(responseBytes)
	}
}

func validateUserLoggedInAction(w http.ResponseWriter, r *http.Request, vars map[string]string) {
	tokenString := vars["token"]
	if "" == tokenString {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(getJwtTokenKey()), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		usr := claims["usr"].(map[string]interface{})

		if nil == usr {
			fmt.Printf("no usr claim present in token \"%s\"\n", tokenString)
			w.WriteHeader(http.StatusUnauthorized)
		}

		if nil != usr && usr["site"] != r.Host {
			fmt.Printf("wrong usr site (%s) in token compared to origin of request (%s)\n", usr["site"], r.RemoteAddr)
			w.WriteHeader(http.StatusUnauthorized)
		}
		// Perform other validation here later by changing _ to claims and investigation fields
		// right now its enough that its valid in time
	} else {
		fmt.Println(err)
		w.WriteHeader(http.StatusUnauthorized)
	}
}

func makeCreateUserAction(uw datasource.UserWriter) func(http.ResponseWriter, *http.Request, map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, vars map[string]string) {

		b, err := ioutil.ReadAll(r.Body)
		user := model.User{}
		err = json.Unmarshal(b, &user)
		if nil != err {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println("could not decode request body correctly")
			return
		}
		if "" == user.Username || "" == user.Password {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println("missing information in user creation body")
			return
		}

		hashedPassword, err := security.HashAndSalt([]byte(user.Password))

		if nil != err {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println("bad password")
			return
		}
		user.Password = hashedPassword
		err = uw.Create(user)

		if nil != err {
			fmt.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// MakeUserLoginBody returns a valid body for making a login request
func MakeUserLoginBody(u string, p string) LoginBody {
	return LoginBody{
		Username: u,
		Password: p,
	}
}

// MakeUserLoginResponse ...
func MakeUserLoginResponse(b []byte) (LoginResponse, error) {
	r := LoginResponse{}
	err := json.Unmarshal(b, &r)
	return r, err
}

// MakeUserLoginHandler creates the handler for user login
func MakeUserLoginHandler(r datasource.UserReader) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/user/login",
		Name: "user-login-route",
		Post: makeLoginUserAction(r),
	}
}

// MakeUserValidateHandler creates the handler for user jwt validation
func MakeUserValidateHandler() *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/user/validate/{token}",
		Name: "user-validation-route",
		Get:  validateUserLoggedInAction,
	}
}

// MakeUsersHandler creates the handler for user creation
func MakeUsersHandler(w datasource.UserWriter) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/users",
		Name: "users-route",
		Post: makeCreateUserAction(w),
	}
}

// MakeUserHandlers returns all handlers for users
func MakeUserHandlers() []*rest.BaseHandler {
	r := datasource.MakeUserJSONReader("./auth_data/users.json")
	w := datasource.MakeUserJSONWriter("./auth_data/users.json", r)
	return []*rest.BaseHandler{
		MakeUsersHandler(w),
		MakeUserLoginHandler(r),
		MakeUserValidateHandler(),
	}
}
