package handler

import (
	"net/http"
	"os"
)

func getJwtTokenKey() string {
	return os.Getenv("AUTH_SERVICE_JWT_TOKEN_KEY")
}

func setUnauthorized(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
}
