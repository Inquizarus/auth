FROM golang:alpine as builder

RUN apk update && apk add ca-certificates

COPY . $GOPATH/src/gitlab.com/inquizarus/auth/

WORKDIR $GOPATH/src/gitlab.com/inquizarus/auth 
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/auth

FROM busybox:latest
COPY --from=builder /go/bin/auth /go/bin/auth
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

WORKDIR /go/bin
CMD ./auth