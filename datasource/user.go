package datasource

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/inquizarus/auth/model"
)

// UserReader interface for reading user data from datasource
type UserReader interface {
	GetByUsername(string) (model.User, error)
	GetAll() ([]model.User, error)
}

// UserWriter ...
type UserWriter interface {
	Create(model.User) error
}

// JSONFileUserWriter writes data about users from a json file
type JSONFileUserWriter struct {
	Reader   UserReader
	FilePath string
}

// Create writes the user to json file
func (w *JSONFileUserWriter) Create(u model.User) error {
	_, err := w.Reader.GetByUsername(u.Username)
	if nil == err {
		return fmt.Errorf("user with username %s already exist", u.Username)
	}

	users, err := w.Reader.GetAll()

	if nil != err {
		return err
	}

	// add new user to the slice of users
	users = append(users, u)

	// write the users to file again
	nb, err := json.Marshal(users)
	if nil != err {
		return err
	}

	err = ioutil.WriteFile(w.FilePath, nb, 0644)

	if nil != err {
		return err
	}

	return nil
}

// JSONFileUserReader is used for users data stored on disk
type JSONFileUserReader struct {
	FilePath string
}

// GetByUsername tries to locate a user from a JSON file
func (r *JSONFileUserReader) GetByUsername(u string) (model.User, error) {
	b, err := ioutil.ReadFile(r.FilePath)

	if nil != err {
		panic(err)
	}

	users := []model.User{}
	err = json.Unmarshal(b, &users)

	if nil != err {
		panic(err)
	}

	for _, user := range users {
		if user.Username == u {
			return user, nil
		}
	}

	return model.User{}, fmt.Errorf("user with username %s does not exist", u)
}

// GetAll returns all users from json file
func (r *JSONFileUserReader) GetAll() ([]model.User, error) {
	users := []model.User{}
	b, err := ioutil.ReadFile(r.FilePath)

	if nil != err {
		panic(err)
	}

	err = json.Unmarshal(b, &users)

	if nil != err {
		panic(err)
	}
	return users, nil
}

// MakeUserJSONReader which loads its data from passed file path when queried
func MakeUserJSONReader(p string) UserReader {
	return &JSONFileUserReader{
		FilePath: p,
	}
}

// MakeUserJSONWriter which writes its data to passed file path when queried
func MakeUserJSONWriter(p string, r UserReader) UserWriter {
	if nil == r {
		r = &JSONFileUserReader{
			FilePath: p,
		}
	}
	return &JSONFileUserWriter{
		FilePath: p,
		Reader:   r,
	}
}
