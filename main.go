package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"gitlab.com/inquizarus/auth/handler"
	"gitlab.com/inquizarus/rest"
)

func main() {
	config := rest.ServeConfig{
		Port:     "8080",
		Handlers: getHandlers(),
		Middlewares: []rest.Middleware{
			withRequestLogging(os.Stdout),
			rest.WithJSONContent(),
		},
	}
	rest.Serve(config)
}

func withRequestLogging(t io.Writer) rest.Middleware {
	return func(nmw http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			format := "%s %s %s\n"
			text := fmt.Sprintf(format, time.Now().Format("2006-01-02 15:04:05 MST"), r.Method, r.RequestURI)
			t.Write([]byte(text))
			// Call the next middleware/handler in chain
			nmw(w, r)
		}
	}
}

func getHandlers() []rest.Handler {
	handlers := []rest.Handler{}
	handlersContainer := [][]*rest.BaseHandler{
		handler.MakeRootHandlers(),
		handler.MakeUserHandlers(),
		handler.MakeAppHandlers(),
	}
	for _, hs := range handlersContainer {
		for _, hsi := range hs {
			handlers = append(handlers, hsi)
		}
	}
	return handlers
}
