module gitlab.com/inquizarus/auth

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	gitlab.com/inquizarus/rest v0.0.0-20190627081554-af3dc00c097f
	golang.org/x/crypto v0.0.0-20181009213950-7c1a557ab941
)
