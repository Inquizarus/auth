package security

import "golang.org/x/crypto/bcrypt"

// HashAndSalt passed password and return a string
func HashAndSalt(password []byte) (string, error) {
	h, err := bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
	if nil != err {
		return "", err
	}
	return string(h), err
}

// VerifyPassword compares two passwords and return true if the are the same
func VerifyPassword(hashed []byte, plain []byte) bool {
	err := bcrypt.CompareHashAndPassword(hashed, plain)
	if nil != err {
		return false
	}
	return true
}
